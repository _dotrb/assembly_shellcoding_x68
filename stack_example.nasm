; stack implementation

global _start

section .text

_start:

	mov eax, 0x66778899
	mov ebx, 0x0
	mov ecx, 0x0

	; push and pop of r/m16 r/m32
	; register push and pop
	
	push ax ; pushing 16 bite values
	pop bx

	push eax ; pushing 32 bite values
	pop ebx

	; memory push and pop

	push word [sample] ; pushing and poping using memory
	pop ecx

	push dword [sample]
	pop edx

	; exit the prog gracfully

	mov eax, 1
	mov ebx, 0
	int 0x80

section .data

	sample: db 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x11, 0x22
