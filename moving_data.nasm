; moving data assembly 

global _start

section .text
_start:
	; mov immediate data to register
	
	mov eax, 0xaaaaaaaaa ; mov "0xaaaaaaaa" to the entier eax register
	mov al, 0xbb ; mov "0xbb" to the lower 8 bits of eax "al"
	mov ah, 0xcc ; mov "0xcc" to the higher 8 bits of eax "ah"
	mov ax, 0xdddd ; mov "0xdddd" to the lower 16 bits of eax "ax"

	mov ebx, 0
	mov ecx, 0 ; put ecx and ebx in the 0 value to use them in the next part


	; mov register to register
	
	mov ebx, eax ; mov the eax value "0xaaaaaaaa" to the ebx entier register
	mov cl, al ; mov the al value "0xbb" to the cl
	mov ch, ah ; mov the ah value "0xcc" to the ch
	mov cx, ax ; mov the ax value "0xdddd" to the lower 16 bits of the ecx "cx"

	; mov from memory to register

	mov al, [sample] ; sample is a label a var we define it in the data section and here we are moving the value of sample to the al
	mov ah, [sample +1] 
	mov bx, [sample]
	mov ecx, [sample]


	; mov from register to memory

	mov eax, 0x33445566 ; ???!!!!
	mov byte [sample], al ; byte is one byte 
	mov word [sample], ax ; word is 2 bytes
	mov dword [sample], eax ; dword is 4 bytes

	; mov immediate data to memory

	mov dword [sample], 0x33445566 ; over writing the first 4 bytes of sample 

	; lea demo 
	
	lea eax, [sample] ; loading sample in the eax register
	lea ebx, [eax] ; loading sample on the ebx register using eax register

	; xchg demo 
	
	mov eax, 0x11223344
	mov ebx, 0xaabbccdd ; we moved two values in these two registers than we are going to exchange these values between the last two registers by calling xchg function

	xchg ebx, eax ; exchanging values between registers

	; exit the program
	
	mov eax, 1 ; sys call to exit
	mov ebx, 0 ; returned value
	int 0x80 ; call kernel

section .data

sample: db 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x11, 0x22 ; sample var with its values
