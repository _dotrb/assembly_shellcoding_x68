#!/bin/bash

echo "(+) assembling ..."
nasm -f elf32 -o $1.o $1.nasm

echo "(+) linking ..."
ld -o $1 $1.o
echo "(+) Done!"
