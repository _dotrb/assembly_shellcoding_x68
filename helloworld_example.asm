; hello world

global _start ; global var to tell the linker this is an entary point

section .text

_start: ; telling the linker where's the entary point

	; printing into screen 
	mov eax, 0x4 ; mov is reposnsible of moving data to register 0x4 or 4 is to sys write eax is register rispensible to sys calls
	mov ebx, 0x1 ; ebx for stdout and 1 is its value
	mov ecx, message ; ecx is pointing to hello world and we replace the string with the var that defines it
	mov edx, mlen ; the edx is pointing to the message length we can count it and put the value or we can use a var in the data section
	int 0x80 ; call kernel

	; exit the  program gracefully
	mov eax, 0x1 ; sys call to exit
	mov ebx, 0x5 ; the return value
	int 0x80 ; call kernel



section .data

	message: db  "helloworld!" ; var is message and db is define byte somth nasm understands it to make a string hello world
	mlen 	equ $-message ; this is to put the length in var eq means equal and $-message is pointing to the length of message var
